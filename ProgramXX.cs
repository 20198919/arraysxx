﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BancoPara3
{
    class cliente
    {

        public string Nombre;
        public int Dinero;

        public cliente(string nom)
        {
            Nombre = nom;
            Dinero = 0;
        }

        public void depositar(int m)
        {
            Dinero = Dinero + m;
        }

        public void Retiro(int m)
        {
            Dinero = Dinero - m;
        }

        public int RegresarMonto()
        {
            return Dinero;
        }

        public void Imprimir()
        {
            Console.WriteLine("La suma del dinero depositado de " + Nombre + " es de " + Dinero);
        }
    }

    class banco
    { 
        private cliente cliente, cliente1, cliente2;
    

    public banco()

    {
        cliente = new cliente("Alex");
            cliente1 = new cliente("Sarah");
            cliente2 = new cliente("Lea");
    }
        public void Operar()
        {
            cliente.depositar(100);
            cliente1.depositar(150);
            cliente2.depositar(200);
            cliente2.Retiro(150);
        }

        public void depositostotales()
        {
            int Total = cliente.RegresarMonto() +
                cliente1.RegresarMonto() +
                cliente2.RegresarMonto();
            Console.WriteLine("El dinero total del banco es:" + Total);
            cliente.Imprimir();
            cliente1.Imprimir();
            cliente2.Imprimir();

        }

        static void Main(string[] args)
        {
            banco banco1 = new banco();
            banco1.Operar();
            banco1.depositostotales();
            Console.ReadKey();
        }

    }
}
